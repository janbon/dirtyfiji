# README #
* For python to Fiji task automation.

* This README explain how to install and use dirtyfiji package.   
	The name tell the quality. Still it work (tested on windows).
	
* It was done due to some issue of reliability with python 2.7 in the still super usefull for newbie Fijibin Package.

### What is this repository for? ###

* Contain the package
* Version 0.1


### How to install ###
* Get Fiji at: https://imagej.net/Fiji/Downloads  
	Install it
	
* Look where is Fiji installed, later we will need the path to the ImageJ-***.exe  
	We will call it the FIJI_PATH. It is like: C:\Users\charli\.bin\Fiji-20141125.app\ImageJ-win64.exe

* Go into a temporary folder with a command prompt (with cd ..), then type in a command prompt:  
	git clone https://bitbucket.org/janbon/dirtyfiji

* It will create a folder into the directory you command prompt is named dirtyfiji,  
	go into the directory dirtyfiji\dirtyfiji
	
* with a text editor open the file named config.py and modify it with YOUR FIJI_PATH for example:  
	configuration['path']='C:\Users\charli\.bin\Fiji-20141125.app\ImageJ-win64.exe'
	
* Save the new config.py at the same place, with the same name.

* Go back to your command prompt  and type:  
	cd dirtyfiji  
	python setup.py install

### How to use ###
* In your python code import dirtyfiji

* dirtyfiji.macro.run(macro_string)  
	will execute the macro_string written as a string in Fiji macro programming language.
	
* dirtyfiji.testdirty()  
	will execute a simple macro writting a string, in order to check if the package works.

### Who do I talk to? ###

![Grid.png](https://bitbucket.org/repo/oLL6ngX/images/3559042265-Grid.png)
* Charles Fosseprez
* charles.fosseprez (at) cri-paris.org