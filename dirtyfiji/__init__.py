# encoding: utf-8

"""This is dirtyfiji

	Allow automation of Fiji Macro in python code

.. moduleauthor:: Charles Fosseprez

.. contact:: charles.fosseprez@cri-paris.org

.. inspiration:: fijibin (https://pypi.python.org/pypi/fijibin/0.3.0)
"""


