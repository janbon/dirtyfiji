# encoding: utf-8

import subprocess, os
from tempfile import mkstemp


import config as config
global path
path=config.configuration['path']


##
# Running macros
##
def run(macro):
	"""
    Runs Fiji with the suplied macro. 

    Parameters
    ----------
    macro : string or list of strings
        IJM-macro(s) to run. If list of strings, it will be joined with
        a space, so all statements should end with ``;``.

    """




	fptr, temp_filename = mkstemp(suffix='.ijm')
	m = os.fdopen(fptr, 'w')
	m.write(macro)
	m.flush() # make sure macro is written before running Fiji
	m.close()

	cmd = [path, '--headless', '-macro', temp_filename]
    
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,
	                            stderr=subprocess.PIPE)

    out, err = proc.communicate()

    # only delete if everything is ok
    os.remove(temp_filename)