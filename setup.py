
from distutils.core import setup

setup(name='dirtyfiji',
      version='0.1',
      description='Fiji macro automation',
      url='',
      author='Charles Fosseprez',
      author_email='charles.fosseprez@cri-paris.org',
      license='MIT',
      packages=['dirtyfiji'],
      zip_safe=False)


